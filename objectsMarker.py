import os
import cv2

# TODO: remove regions by right click
# TODO: show help by F1
# TODO: refactor: make a class MarkingController from code in main()

class ImageSampleMarkerFrame:
    def __init__(self, imageFileName, regions = None, windowName = "Frame"):
        self.fileName = imageFileName
        self.regions = regions if regions is not None else []
        self.cropping = False
        self.windowName = windowName
        self.moveDirection = 1

    def start(self):
        self.frame = cv2.imread(self.fileName)
        # show it
        cv2.imshow("Frame", self.frame)
        cv2.namedWindow("Frame")
        cv2.setWindowTitle("Frame", self.windowName)
        cv2.setMouseCallback("Frame", self.mouseEventHandler)
        self.redraw()

        # handle keyboard events
        while True:
            key = cv2.waitKeyEx(1)

            if key == ord('\b'):
                self.regions.pop(-1)
                self.redraw()

            elif key == ord('\r') or key == 2555904: # Enter or right arrow:
                cv2.destroyWindow("Frame")
                return self.regions if len(self.regions) > 0 else None

            elif key == 27:
                cv2.destroyWindow("Frame")
                return None

            elif key == 2424832: # left arrow
                cv2.destroyWindow("Frame")
                self.moveDirection = -1
                return self.regions if len(self.regions) > 0 else None

            elif key == ord('q'):
                cv2.destroyWindow("Frame")
                exit(0)

            elif key != -1:
                print(key)

    def mouseEventHandler(self, event, x, y, flags, param):
        # if the left mouse button was clicked, record the starting
        # (x, y) coordinates and indicate that cropping is being
        # performed
        if event == cv2.EVENT_LBUTTONDOWN:
            self.firstPoint = (x, y)
            self.cropping = True

        # check to see if the left mouse button was released
        elif event == cv2.EVENT_LBUTTONUP:
            # record the ending (x, y) coordinates and indicate that
            # the cropping operation is finished
            regionToAdd = self.normalizeRegion((self.firstPoint[0], self.firstPoint[1], x, y))
            self.regions.append(regionToAdd)
            self.cropping = False
            self.redraw()

    def redraw(self):
        # draw a rectangle around the region of interest
        frameCopy = self.frame.copy()
        for region in self.regions:
            cv2.rectangle(frameCopy, (region[0], region[1]), (region[2], region[3]), (0, 255, 0), 2)
        cv2.imshow("Frame", frameCopy)

    def normalizeRegion(self, region):
        return (min(region[0], region[2]), min(region[1], region[3]), max(region[0], region[2]), max(region[1], region[3]))

    def getMoveDirection(self):
        return self.moveDirection


def main():
    # Get list of images
    samplesFileName = os.path.join('.', 'samples.dat')
    imagesPath = "./Good"

    allSampleRegions = {}
    if os.path.exists(samplesFileName):
        oldSampleFile = open(samplesFileName, 'r')
        oldSampleLines = oldSampleFile.readlines()
        oldSampleLines = [line[:-1] if line[-1] == '\n' else line for line in oldSampleLines]
        oldSampleLineTokens = [line.split() for line in oldSampleLines]
        oldSampleRegionNumbers = { tokens[0] : [int(token) for token in tokens[2:]] for tokens in oldSampleLineTokens }
        allSampleRegions = {filename : [tuple(numbers[i:i+4]) for i in range(0, len(numbers), 4)] for filename, numbers in oldSampleRegionNumbers.items()}
        oldSampleFile.close()

    # TODO: we have to validate loaded mark lines

    fileNames = [os.path.join(imagesPath, name) for name in os.listdir(imagesPath)]

    imageIndex = 0
    while True:
        imageName = fileNames[imageIndex]

        imageRegions = allSampleRegions[imageName] if imageName in allSampleRegions else None
        markerWindowTitle = imageName + ' - ' + str(imageIndex+1) + '/' + str(len(fileNames))
        frameMarker = ImageSampleMarkerFrame(imageName, imageRegions, markerWindowTitle)
        sampleRegions = frameMarker.start()

        print(sampleRegions)
        if not sampleRegions is None:
            allSampleRegions.update({imageName : sampleRegions})

            allSampleLines = [str(filename+' '+str(len(markRegions))+' '+' '.join(str(x) for region in markRegions for x in region)) for filename, markRegions in allSampleRegions.items()]
            resultFile = open(samplesFileName, 'w')
            resultFile.writelines([line+'\n' for line in allSampleLines])
            resultFile.flush()
            resultFile.close()

        imageIndex += frameMarker.getMoveDirection()
        if imageIndex < 0:
            imageIndex = imageIndex + len(fileNames)
        imageIndex %= len(fileNames)

if __name__ == "__main__":
    main()